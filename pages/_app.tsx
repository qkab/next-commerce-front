import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client'
import { useEffect, useMemo, useState } from 'react'

import { useApollo } from '../lib/apolloClient'
import { ProductsContext, ShoppingCartContext } from '../contexts/products.context'
import { IProductInfos } from '../apollo/queries/products.query'
import { CategoriesContext } from '../contexts/categories.context'
import Layout from '../components/Layout'

function MyApp({ Component, pageProps }: AppProps) {
  const apolloClient = useApollo(pageProps)
  const [products, setProducts] = useState<IProductInfos[]>([])
  const [categoryId, setCategoryId] = useState("")
  const [shoppingCartItems, setShoppingCartItems] = useState<IProductInfos[]>([])
  const productsProvider = useMemo(() => ({ products, setProducts }), [products, setProducts])
  const categoryIdProvider = useMemo(() => ({ categoryId, setCategoryId }), [categoryId, setCategoryId])
  const shoopingCartProvider = useMemo(() => ({shoppingCartItems, setShoppingCartItems}), [shoppingCartItems, setShoppingCartItems])

  useEffect(() => {
    const storedShoppingCartItems: IProductInfos[] = JSON.parse(localStorage.getItem('shoppingCart') || '[]')
    setShoppingCartItems([...storedShoppingCartItems])

  }, [])

  return (
    <ShoppingCartContext.Provider value={shoopingCartProvider}>
      <CategoriesContext.Provider value={categoryIdProvider}>
        <ProductsContext.Provider value={productsProvider}>
          <ApolloProvider client={apolloClient}>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </ApolloProvider>
        </ProductsContext.Provider>
      </CategoriesContext.Provider>
    </ShoppingCartContext.Provider>
  )
}

export default MyApp
