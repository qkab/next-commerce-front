import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useContext, useEffect, useState } from 'react'
import { IShoppingCartContext, ShoppingCartContext } from '../../contexts/products.context'


const Success = () => {
  const [session, setSession] = useState(null)
  const { setShoppingCartItems } = useContext<IShoppingCartContext>(ShoppingCartContext)
  const router = useRouter()
  const { session_id } = router.query

  useEffect(() => {
    const loadStripeSession = async () => {
      if(session_id) {
        const response = await fetch(`/api/checkout/${session_id}`)
        const data = await response.json()
        setSession(data)
        setShoppingCartItems([])
        localStorage.setItem('shoppingCart', JSON.stringify([]))
      }
    }

    loadStripeSession()
  }, [session_id])

  return (
    <div className='flex flex-col container items-start justify-center h-screen card'>
      <h1 className='font-bold text-xl'>Merci pour votre achat !</h1>
      <p className='text-lg'>Vous allez recevoir un email de confirmation d&apos;ici peu!</p>
      <Link passHref={true} href='/'>
        <p className='rounded-lg p-2 bg-blue-500 text-white w-full text-center mt-3 cursor-pointer'>Accueil</p>
      </Link>
    </div>
  )
}

export default Success
