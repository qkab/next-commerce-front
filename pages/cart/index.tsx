import React, { useContext, useEffect, useState } from 'react'
import groupBy from 'lodash/groupBy'
import uniqBy from 'lodash/uniqBy'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { MinusCircleIcon, PlusCircleIcon, TrashIcon } from '@heroicons/react/outline'
import { loadStripe } from '@stripe/stripe-js'
import { toast, ToastContainer } from 'react-nextjs-toast'

import { IShoppingCartContext, ShoppingCartContext } from '../../contexts/products.context'
import { IShoppingCartItem } from '../../apollo/queries/products.query'

const stripePromise = loadStripe(process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY!)

const Cart = () => {
  const { shoppingCartItems, setShoppingCartItems } = useContext<IShoppingCartContext>(ShoppingCartContext)

  const [cartItems, setCartItems] = useState<IShoppingCartItem[]>([])
  const [total, setTotal] = useState(0)

  const router = useRouter()
  const { cancel } = router.query


  const getUniqCartItems = () => {
    const filteredCartItems: IShoppingCartItem[] = []
    const newCartItems = groupBy(shoppingCartItems, 'id')
    for (const [itemId, items] of Object.entries(newCartItems)) {
      items.forEach((item) => {
        filteredCartItems.push({ product: item, quantity: items.length, id: itemId })
      })
    }
    return uniqBy(filteredCartItems, 'id')
  }
  const addProduct = (cartItem: IShoppingCartItem) => {
    const updatedCartItems = cartItems.map((item) => {
      if(item.id === cartItem.id) {
        return {...item, quantity: item.quantity + 1 }
      }

      return item
    })
    setCartItems([...updatedCartItems])
    setShoppingCartItems([...shoppingCartItems, cartItem.product])
    localStorage.setItem('shoppingCart', JSON.stringify([...shoppingCartItems, cartItem.product]))
  }

  const substractProduct = (cartItem: IShoppingCartItem) => {
    const updatedCartItems = cartItems.map((item) => {
      if(item.id === cartItem.id) {
        return {...item, quantity: item.quantity - 1 }
      }

      return item
    })
    setCartItems([...updatedCartItems])
    const cartItemIndexToRemove = shoppingCartItems.findIndex((shoppingCartItem) => shoppingCartItem.id === cartItem.id)
    const updatedShoppingCartItems = [...shoppingCartItems]

    updatedShoppingCartItems.splice(cartItemIndexToRemove, 1)

    setShoppingCartItems(updatedShoppingCartItems)
    localStorage.setItem('shoppingCart', JSON.stringify(updatedShoppingCartItems))
  }

  useEffect(() => {
    const uniqCartItems = getUniqCartItems()
    let newTotal = 0

    uniqCartItems.forEach((uniqCartItem) => {
      newTotal = newTotal + (uniqCartItem.product.attributes.price * 100) * uniqCartItem.quantity
    })

    setTotal(newTotal)
    setCartItems([...uniqCartItems])
  }, [shoppingCartItems])
  
  useEffect(() => {
    if(!!cancel) {
      toast.notify('Une erreur est survenue lors de l\'achat', {
        type: 'error',
        title: 'Achat en ligne'
      })
    }
  }, [cancel])

  const handlePay = async () => {
    const stripe = await stripePromise

    const response = await fetch('/api/checkout', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ cartItems })
    })
    const session = await response.json()
    
    await stripe?.redirectToCheckout({ sessionId: session.id })
  }
  return (
    <div className='container mx-auto w-screen h-screen'>
      <ToastContainer />
      <h1 className='font-bold text-lg my-3 sm:text-center sm:text-3xl text-xl'>Panier</h1>
      <ul className="card">
        {cartItems.map((cartItem) => {
          return (
            <li
              key={cartItem.product.id}
              className='flex items-center space-x-4 py-5 border-y'
            >
              <Image
                width={100}
                height={150}
                src={`http://localhost:1337${cartItem.product.attributes.images.data[0].attributes.url}`}
                alt={cartItem.product.attributes.images.data[0].attributes.name}
              />
              <div className="flex flex-col space-y-3">
                <h2 className='font-bold'>{cartItem.product.attributes.name}</h2>
                <span>{cartItem.product.attributes.price.toFixed(2)}€</span>
                <p>Quantité: {cartItem.quantity}</p>
                <div className="flex items-center space-x-3">
                  <button onClick={() => substractProduct(cartItem)}>
                    {
                      cartItem.quantity === 1 ? (<TrashIcon className='w-5 h-5' />) : (<MinusCircleIcon className='w-5 h-5' />)
                    }
                  </button>
                  <button onClick={() => addProduct(cartItem)}>
                    <PlusCircleIcon className='w-5 h-5' />
                  </button>
                </div>
              </div>
            </li>
          )
        })}
        <div className="w-full flex flex-col items-center justify-end space-y-2">
          <span>Sous-total ({cartItems.length} articles):
            <span className='font-bold'>{` ${(total/100).toFixed(2)}€`}</span>
          </span>
          <button
            className='rounded-lg w-1/2 text-lg font-bold p-2 bg-blue-500 text-white'
            onClick={() => handlePay()}
          >
            Payer
          </button>
        </div>
      </ul>
    </div>
  )
}

export default Cart
