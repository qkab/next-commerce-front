// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import Stripe from 'stripe'
import { IShoppingCartItem } from '../../../apollo/queries/products.query'

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY!, {
  apiVersion: '2020-08-27'
})

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Stripe.Response<Stripe.Checkout.Session>>
) {
  if(req.method !== 'POST') {
    res.setHeader('Allow', 'POST')
    res.status(405).end('Method not Allowed')
  }

  try {
    const { cartItems } = req.body
    const line_items: Stripe.Checkout.SessionCreateParams.LineItem[] = cartItems.map((cartItem: IShoppingCartItem) => ({
      quantity: cartItem.quantity,
      price_data: {
        unit_amount: parseInt((cartItem.product.attributes.price * 100).toString(), 10),
        currency: 'EUR',
        product_data: {
          name: cartItem.product.attributes.name,
          description: cartItem.product.attributes.description,
          metadata: {
            productId: cartItem.product.id
          }
        }
      }
    }))

    const session = await stripe.checkout.sessions.create({
      line_items,
      mode: 'payment',
      success_url: `${req.headers.origin}/success?session_id={CHECKOUT_SESSION_ID}`,
      cancel_url: `${req.headers.origin}/cart?cancel=true`,
      payment_method_types: ['card'],
      billing_address_collection: 'required',
      shipping_address_collection: {
        allowed_countries: ['FR']
      }
    })
    res.json(session)
  } catch (error: any) {
    console.log(error)

    res.status(error.statusCode || 500).json(error.message);
  }
}
