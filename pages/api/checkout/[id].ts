// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import Stripe from 'stripe'

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY!, {
  apiVersion: '2020-08-27'
})

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Stripe.Response<Stripe.Checkout.Session>>
) {
  try {
    const { id } = req.query
    const session = await stripe.checkout.sessions.retrieve(id as string)

    res.json(session)
  } catch (error: any) {
    console.log(error)
    res.status(error.statusCode || 500).json(error.message);
  }
}
