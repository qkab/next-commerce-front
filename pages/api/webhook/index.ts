import type { NextApiRequest, NextApiResponse } from 'next'
import Stripe from 'stripe'
import { buffer } from 'micro'
import { GraphQLClient } from 'graphql-request'

import { ORDER_ITEMS_MUTATION, ORDER_MUTATION } from '../../../apollo/mutations/order.mutation'
import { STRIPE_EVENTS } from '../../../strapi/events'
import { IOrderInput, IOrderItemInput, IOrderStatus } from '../../../apollo/interfaces/order.interface'

const stripe = new Stripe(process.env.STRIPE_SECRET_KEY!, {
  apiVersion: '2020-08-27'
})
const endpointSecret: string = process.env.STRIPE_WEBHOOK_SECRET!

export const config = {
    api: {
        bodyParser: false
    }
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
    if(req.method === 'POST') {
        const payload = await buffer(req)
        const signature = req.headers['stripe-signature']

        let event: Stripe.Event

        try {
            event = stripe.webhooks.constructEvent(payload.toString(), signature!, endpointSecret)
        } catch (error: any) {
            return res.status(400).send(`Webhook Error: ${error.message}`)
        }

        switch (event.type) {
            case STRIPE_EVENTS.CHECKOUT_SESSION_COMPLETED:
                const session = event.data.object as Stripe.Checkout.Session
                const retirevedSession = await stripe.checkout.sessions.retrieve(session.id, {
                    expand: ['line_items.data.price.product', 'customer']
                })

                const line_items = retirevedSession.line_items!.data
                const customer = retirevedSession.customer as Stripe.Customer

                const client = new GraphQLClient(process.env.GRAPHQL_API!)
                const amount = retirevedSession.amount_total! / 100
                const order_items = line_items.map((item) => {
                    const product: Stripe.Product = item.price?.product as Stripe.Product
                    return {
                        quantity: item.quantity,
                        total: parseFloat((item.price!.unit_amount! / 100).toFixed(2).toString()),
                        product: product.metadata.productId
                    }
                })

                const orderItemIds: string[] = []
                
                for (const item of order_items) {
                    try {
                        const orderItemVariables: IOrderItemInput = {
                            data: {
                                quantity: item.quantity!,
                                total: item.total,
                                product: item.product
                            }
                        }
                        const orderItems = await client.request(ORDER_ITEMS_MUTATION, orderItemVariables)
                        const { createOrderItem: {data: { id: orderItemId }} } = orderItems
                        orderItemIds.push(orderItemId)
                    } catch (error: any) {
                        console.log("=====================ORDER ITEM CREATION=====================");
                        console.log({ error: error.message });
                    }
                }

                try {
                    const orderVariables: IOrderInput = {
                        data: {
                            checkout_session_id: retirevedSession.id,
                            status: retirevedSession.payment_status as IOrderStatus,
                            price: parseFloat(amount.toFixed(2).toString()),
                            order_items: orderItemIds,
                            email: customer.email!
                        }
                    }
                    await client.request(ORDER_MUTATION, orderVariables)
                } catch (error: any) {
                    console.log("=====================ORDER CREATION=====================");
                    console.log({ error: error.message });
                }
                break  
        
            default:
                return res.status(400).send('Unkown Event triggered')
        }

        res.status(200).json({ received: true })
    } else {
        res.setHeader('Allow', 'POST')
        res.status(405).end('Method Not Allowed')
    }

}
