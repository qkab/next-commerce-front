import type { NextPage } from 'next'
import Head from 'next/head'
import { ALL_CATEGORIES_QUERY } from '../apollo/queries/categories.query'
import { ALL_PRODUCTS_QUERY } from '../apollo/queries/products.query'
import Categories from '../components/Categories'
import Products from '../components/Products'
import { addApolloState, initializeApolloClient } from '../lib/apolloClient'

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Next Commerce</title>
        <meta name="description" content="Site e-commerce de vêtements" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="container mx-auto">        
        {/* Catégories */}
        <div className="flex w-full">
          <Categories />
        </div>

        {/* Products */}
        <div className="flex w-full">
          <Products />
        </div>

      </main>
    </>
  )
}

export async function getStaticProps() {
  const apolloClient = initializeApolloClient()

  await apolloClient.query({
    query: ALL_CATEGORIES_QUERY,
    notifyOnNetworkStatusChange: true
  })
  await apolloClient.query({
    query: ALL_PRODUCTS_QUERY,
    notifyOnNetworkStatusChange: true
  })

  return addApolloState(apolloClient, {
    props: {},
    revalidate: 1
  })
}

export default Home
