import React, { Dispatch, SetStateAction } from "react";

export interface ICategoriesContext {
    categoryId: string;
    setCategoryId: Dispatch<SetStateAction<string>>;
}

export const CategoriesContext = React.createContext<any>(null)
