import React, { Dispatch, SetStateAction } from "react"
import { IProductInfos } from "../apollo/queries/products.query"

export interface IProductsContext {
    products: IProductInfos[];
    setProducts: Dispatch<SetStateAction<IProductInfos[]>>;
}
export interface IShoppingCartContext {
    shoppingCartItems: IProductInfos[];
    setShoppingCartItems: Dispatch<SetStateAction<IProductInfos[]>>;
}

export const ProductsContext = React.createContext<any>(null)
export const ShoppingCartContext = React.createContext<any>(null)