import React from 'react'
import Header from './Header'

const Layout: React.FC = ({ children }) => {
  return (
    <div className='container mx-auto px-4'>
      <Header />
      {children}
    </div>
  )
}

export default Layout
