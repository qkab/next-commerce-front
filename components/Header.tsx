import { ShoppingBagIcon } from '@heroicons/react/outline'
import { useRouter } from 'next/router'
import React, { useContext, useEffect, useState } from 'react'
import { IShoppingCartContext, ShoppingCartContext } from '../contexts/products.context'

const Header = () => {
  const { shoppingCartItems } = useContext<IShoppingCartContext>(ShoppingCartContext)
  const router = useRouter()

  return (
    <nav className='flex items-center justify-between w-full'>
    <h1 className="text-lg font-bold cursor-pointer" onClick={() => router.push('/')}>Next Commerce</h1>
      <button className="flex items-center" onClick={() => router.push('/cart')}>
        {shoppingCartItems.length > 0 && <span className='bg-blue-500 rounded-full text-white p-1 w-full text-center'>{shoppingCartItems.length}</span>}
        <ShoppingBagIcon className={shoppingCartItems.length > 0 ? 'w-10 h-10' : 'w-5 h-5'} />
      </button>
    </nav>
  )
}

export default Header
