import { ExclamationIcon } from '@heroicons/react/outline'
import React from 'react'

const Error: React.FC<{ message: string }> = ({ message }) => {
    return (
        <div className="flex items-center justify-center w-full">
            <h1 className='flex mx-auto'>
                <ExclamationIcon className="h-6 w-6 text-red-600" aria-hidden="true" />
                {message}
            </h1>
        </div>
    )
}

export default Error
