import React, { useContext, useEffect } from 'react'
import { ShoppingCartIcon } from '@heroicons/react/outline'
import { NetworkStatus, useQuery } from '@apollo/client'
import { ALL_PRODUCTS_QUERY, IProductInfos, IProductQuery } from '../apollo/queries/products.query'
import { IProductsContext, IShoppingCartContext, ProductsContext, ShoppingCartContext } from '../contexts/products.context'
import Error from './Error'
import Loading from './Loading'
import { CategoriesContext, ICategoriesContext } from '../contexts/categories.context'

const Products = () => {
    const { loading, error, data, fetchMore, networkStatus } = useQuery<IProductQuery>(ALL_PRODUCTS_QUERY, { notifyOnNetworkStatusChange: true })
    const { products, setProducts } = useContext<IProductsContext>(ProductsContext)
    const { categoryId } = useContext<ICategoriesContext>(CategoriesContext)
    const { shoppingCartItems, setShoppingCartItems } = useContext<IShoppingCartContext>(ShoppingCartContext)
    const loadingMoreProducts = networkStatus === NetworkStatus.fetchMore
    
    if(error) return (
        <Error message='Erreur pendant le chargement des produits.' />
    )

    if(loading && !loadingMoreProducts) return (
        <Loading message='Chargement...' />
    )

    const { data: allProducts, meta } = data!.products

    useEffect(() => {
        if(categoryId) {
            const filteredProducts = allProducts.filter((product) => product.attributes.categories.data.find((cat) => cat.id === categoryId))    
            setProducts([...filteredProducts])
        } else {
            setProducts([...allProducts])
        }
    }, [allProducts, categoryId])

    const areMoreProducts = allProducts.length < meta.pagination.total

    const loadMoreProducts = () => fetchMore({
        variables: { skip: allProducts.length }
    })

    const addToCart = (product: IProductInfos) => {
        const newShoppingCartItems = [product, ...shoppingCartItems]
        setShoppingCartItems(newShoppingCartItems)
        localStorage.setItem('shoppingCart', JSON.stringify(newShoppingCartItems))
    }

    return (
        <section className='w-full my-5'>
            <h2 className='text-lg font-bold underline sm:text-2xl'>Products</h2>
            {!products.length && (
                <h4>Aucun produit disponible</h4>
            )}
            <ul className='flex flex-col justify-center h-full md:grid md:grid-cols-2 md:gap-5'>{products.map((product) => {
                const [image] = product.attributes.images.data
                return (
                    <li
                        key={product.id}
                        className='card flex flex-col h-full my-3 md:w-full md:flex-row md:space-y-0 md:space-x-12 sm:space-x-2'
                    >
                        <img
                            src={`http://localhost:1337${image.attributes.url}`}
                            alt={image.attributes.name}
                            className='md:w-56 md:h-72'
                        />
                        <div className='md:h-60 sm:w-full'>
                            <div className="flex flex-col my-5 md:space-y-3">
                                <h4 className='font-bold text-lg'>{product.attributes.name}</h4>
                                <p className='text-sm'>{product.attributes.description}</p>
                                <span className='font-bold'>{product.attributes.price.toFixed(2)}€</span>
                            </div>
                            <div className="flex items-center space-x-3 sm:space-y-3 sm:flex-col md:mt-5">
                                <button className='border sm:w-full sm:p-1 md:p-3 rounded-full p-2 text-sm font-bold'>En savoir plus</button>
                                <button
                                    className='flex border sm:w-full sm:p-1 md:p-3 justify-center bg-blue-500 text-white items-center rounded-full p-2 space-x-3 text-sm font-bold'
                                    onClick={() => addToCart(product)}
                                >
                                    <ShoppingCartIcon className='w-5 h-5 mr-1' />
                                    Ajouter au panier
                                </button>
                            </div>
                        </div>
                    </li>
                )
            })}</ul>

            {areMoreProducts && (
                <button
                    className="w-5 h-5"
                    onClick={() => loadMoreProducts()}
                    disabled={loadingMoreProducts}
                >
                    {loadingMoreProducts ? 'Chargement...' : 'Afficher plus'}
                </button>
            )}
        </section>
    )
}

export default Products
