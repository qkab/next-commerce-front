import { NetworkStatus, useQuery } from '@apollo/client'
import React, { useContext } from 'react'

import { ALL_CATEGORIES_QUERY, ICategoryInfos, ICategoryQuery } from '../apollo/queries/categories.query'
import Error from './Error'
import Loading from './Loading'
import { CategoriesContext, ICategoriesContext } from '../contexts/categories.context'

const Categories = () => {
    const { loading, error, data, fetchMore, networkStatus } = useQuery<ICategoryQuery>(ALL_CATEGORIES_QUERY, { notifyOnNetworkStatusChange: true })
    const { categoryId, setCategoryId } = useContext<ICategoriesContext>(CategoriesContext)
    const loadingMoreCategories = networkStatus === NetworkStatus.fetchMore

    if(error) return (
        <Error message='Erreur pendant le chargement des catégories.' />
    )

    if(loading && !loadingMoreCategories) return (
        <Loading message='Chargement...' />
    )

    const { data: allCategories, meta } = data!.categories
    const areMoreCategories = allCategories.length < meta.pagination.total

    const loadMoreCategories = () => fetchMore({
        variables: { skip: allCategories.length }
    })

    const selectACategory = (category: string) => {
        if(categoryId !== category) {
            setCategoryId(category)
        } else {
            setCategoryId('')
        }
    }

    return (
        <section  className='w-full my-5'>
            <h2 className='text-lg font-bold underline sm:text-2xl'>Catégories</h2>
            <ul className='flex flex-wrap items-start w-full space-x-3 mt-5'>{allCategories.map((category: ICategoryInfos) => (
                <li
                    key={category.id}
                    className={`border-2 p-2 rounded-lg sm:hover:bg-black sm:hover:text-white cursor-pointer ${categoryId === category.id ? 'bg-black text-white' : ''}`}
                    onClick={() => selectACategory(category.id)}
                >
                    <h3 className='font-bold text-sm'>{category.attributes.name}</h3>
                </li>
            ))}</ul>
            {areMoreCategories && (
                <button
                    className="w-5 h-5"
                    onClick={() => loadMoreCategories()}
                    disabled={loadingMoreCategories}
                >
                    {loadingMoreCategories ? 'Chargement...' : 'Afficher plus'}
                </button>
            )}
        </section>
    )
}

export default Categories
