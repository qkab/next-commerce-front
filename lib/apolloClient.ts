import { ApolloClient, HttpLink, InMemoryCache, NormalizedCacheObject } from "@apollo/client"
import { concatPagination } from "@apollo/client/utilities"
import { useMemo } from "react"
import merge from "deepmerge"
import isEqual from "lodash/isEqual"

export const APOLLO_STATE_PROP_NAME = '__APOLLO_STATE__'

let apolloClient: ApolloClient<NormalizedCacheObject>
const isSsrMode = typeof window === 'undefined'

const createApolloClient = () => {
    return new ApolloClient({
        ssrMode: isSsrMode,
        link: new HttpLink({
            uri: 'http://localhost:1337/graphql',
            credentials: 'same-origin',
        }),
        cache: new InMemoryCache({
            addTypename: false,
            typePolicies: {
                Query: {
                    fields: { allPosts: concatPagination() }
                }
            }
        })
    })
}

export const initializeApolloClient = (initialState = null) => {
    const _apolloClient = apolloClient ?? createApolloClient()

    if(initialState) {
        const existingCache = _apolloClient.extract()

        const data = merge(initialState, existingCache, {
            arrayMerge: (destinationArray, sourceArray) => [
                ...sourceArray,
                ...destinationArray.filter((d) => sourceArray.every((s) => !isEqual(d, s)))
            ]
        })

        _apolloClient.cache.restore(data)
    }

    if(isSsrMode) return _apolloClient
    if(!apolloClient) apolloClient = _apolloClient

    return _apolloClient
}

export function addApolloState(client: ApolloClient<NormalizedCacheObject>, pageProps: any) {
    if (pageProps?.props) {
      pageProps.props[APOLLO_STATE_PROP_NAME] = client.cache.extract()
    }
  
    return pageProps
}
  
export const useApollo = (pageProps: any) => {
    const state = pageProps[APOLLO_STATE_PROP_NAME]
    const store = useMemo(() => initializeApolloClient(state), [state])
    return store
}