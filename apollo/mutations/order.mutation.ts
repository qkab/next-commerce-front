import { gql } from 'graphql-request'

export const ORDER_ITEMS_MUTATION = gql`
mutation createOrderItem($data: OrderItemInput!){
    createOrderItem(data: $data) {
        data {
            id
        }
    }
}
`
export const ORDER_MUTATION = gql`
mutation createOrder($data: OrderInput!){
    createOrder(data: $data) {
        data {
            id
            attributes {
                checkout_session_id
                status
                price
                email
                createdAt
                order_items {
                    data {
                        id
                        attributes {
                            quantity
                            total
                            product {
                                data {
                                    id
                                    attributes {
                                        name
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
`