export enum IOrderStatus {
    PAID = 'paid',
    UNPAID = 'unpaid'
}
export interface IOrderInput {
    data: {
        checkout_session_id: string,
        status: IOrderStatus,
        price: number,
        order_items: string[],
        email: string
    }
}

export interface IOrderItemInput {
    data: {
        quantity: number,
        total: number,
        product: string
    }
}