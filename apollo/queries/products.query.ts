import { gql } from "@apollo/client";
import { ICategoryInfos } from "./categories.query";

export interface IShoppingCartItem {
	id: string
	product: IProductInfos
	quantity: number
}
export interface IImageInfos {
	id: string
	attributes: {
			name: string
			url: string
	}
}

export interface IImage {
	data: IImageInfos[]
}
export interface IProductInfos {
	id: string
	attributes: {
		name: string
		description: string
		price: number
		images: IImage
		slug: string
		categories: { data: ICategoryInfos[] }
	}
}

export interface IProduct {
	data: IProductInfos[]
	meta: {
		pagination: {
				total: number
		}
	}
}

export interface IProductQuery {
	products: IProduct
}

export const ALL_PRODUCTS_QUERY = gql`
query {
	products {
		data {
			id
			attributes {
				name
				description
				price
				images {
					data {
						id
						attributes {
							name
							url
						}
					}
				}
				slug
				categories {
					data {
						id
						attributes {
							name
						}
					}
				}
			}
		}
		meta {
			pagination {
				total
			}
		}
	}
}
`