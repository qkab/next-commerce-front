import { gql } from "@apollo/client";

export interface ICategoryInfos {
    id: string
    attributes: {
        name: string
    }
}

export interface ICategory {
    data: ICategoryInfos[]
    meta: {
        pagination: {
            total: number
        }
    }
}


export interface ICategoryQuery {
    categories: ICategory
}

export const ALL_CATEGORIES_QUERY = gql`
query {
	categories {
		data {
			id
			attributes {
				name
				products {
					data {
						id
						attributes {
							name
							description
							price
						}
					}
				}
				createdAt
			}
		}
		meta {
			pagination {
				pageCount
				total
			}
		}
	}
}
`